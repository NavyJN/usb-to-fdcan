#include "qlk_fdcan.h"
#include "usb_device.h"
#include "fdcan.h"


uint8_t fdcan2_rdata[FDCAN_BUF_LEN] = {0};  // FDCAN2 接收数据缓存
uint8_t fdcan2_tdata[FDCAN_BUF_LEN] = {0};  // FDCAN2 发送数据缓存
fdcan_queue fdca2_queue = // FDCAN2 队列
{
    .front = 0,
    .rear = 0,
};
p_fdcan_queue p_fdcan2_queue = &fdca2_queue;  // FDCAN2 队列指针


/**
 * @brief 根据数据长度计算 FDCAN 的 DLC
 * @param len 数据长度
 * @return FDCNA 的 DLC
 */
uint32_t Fdcan_Len_To_Dlc(uint8_t len)
{
    uint32_t dlc = 0;

    if (len <= 8)
    {
        dlc = len << 16;
    }
    else if (len <= 12)
    {
        dlc = FDCAN_DLC_BYTES_12;
    }
    else if (len <= 16)
    {
        dlc = FDCAN_DLC_BYTES_16;
    }
    else if (len <= 20)
    {
        dlc = FDCAN_DLC_BYTES_20;
    }
    else if (len <= 24)
    {
        dlc = FDCAN_DLC_BYTES_24;
    }
    else if (len <= 32)
    {
        dlc = FDCAN_DLC_BYTES_32;
    }
    else if (len <= 48)
    {
        dlc = FDCAN_DLC_BYTES_48;
    }
    else if (len <= 64)
    {
        dlc = FDCAN_DLC_BYTES_64;
    }

    return dlc;
}


/**
 * @brief 跟据 FDCAN 的 DLC 计算数据长度
 * @param dlc FDCAN 的 DLC
 * @return 数据长度
 */
uint8_t Fdcan_Dlc_To_Len(uint32_t dlc)
{
    uint8_t len = 0;
    uint8_t tab_dlc_to_len[] = {12, 16, 20, 24, 32, 48, 64};

    if (dlc <= FDCAN_DLC_BYTES_8)
    {
        len = dlc >> 16;
    }
    else
    {
        len = tab_dlc_to_len[(dlc >> 16) - 9];
    }

    return len;
}


/**
 * @brief 将 FDCAN 数据入队
 * @param p_fdcan_queue 队列指针
 * @param data 要写入的数据
 * @param len 写入数据的长度
 * @return 0: 成功，
 *        -1: 指针为空，
 *        -2: 要写入的数据过长
 */
int8_t Fdcan_Queue_Push(p_fdcan_queue p_fdcan_queue, uint8_t *data, uint8_t len)
{
    if (p_fdcan_queue == NULL || data == NULL)
    {
        DEBUG_PRINT("指针为空！！！");
        return -1;
    }

    if (len <= 0 || len > FDCAN_BUF_LEN)
    {
        DEBUG_PRINT("数据长度错误！！！");
        return -2;
    }

    DEBUG_PRINT("p_fdcan_queue->front = %d\n", p_fdcan_queue->front);

    memcpy(&p_fdcan_queue->data[p_fdcan_queue->front].data[0], data, len);
    p_fdcan_queue->data[p_fdcan_queue->front].len = len;
    p_fdcan_queue->front = (p_fdcan_queue->front + 1) % FDCAN_QUEUE_SIZE;
    
    /* 防止入队速度大于出队速度导致整个队列的数据丢失 */
    if (p_fdcan_queue->front == p_fdcan_queue->rear)
    {
        p_fdcan_queue->rear = (p_fdcan_queue->rear + 1) % FDCAN_QUEUE_SIZE;
    }

    return 0;
}


/**
 * @brief 将数据从队列中读出并放入 data，并返回读出数据的长度
 * @param p_fdcan_queue 队列指针
 * @param data 读出数据放入的数组
 * @return >0: 成功读出数据的长度，
 *         -1: 指针为空，
 *         -2: 队列为空,
 */
int8_t Fdcan_Queue_Pop(p_fdcan_queue p_fdcan_queue, uint8_t *data)
{
    if (p_fdcan_queue == NULL || data == NULL)
    {
        DEBUG_PRINT("指针为空！！！");
        return -1;
    }

    if (p_fdcan_queue->rear == p_fdcan_queue->front)
    {
        DEBUG_PRINT("队列为空！！！");
        return -2;
    }
	
	DEBUG_PRINT("p_fdcan_queue->rear = %d\n", p_fdcan_queue->rear);

    memcpy(data, &p_fdcan_queue->data[p_fdcan_queue->rear].data, p_fdcan_queue->data->len);

    p_fdcan_queue->rear = (p_fdcan_queue->rear + 1) % FDCAN_QUEUE_SIZE;

    return p_fdcan_queue->data->len;
}


/**
 * @brief 从队列中取出数据并通过串口发送
 * @param p_fdcan_queue 队列指针
 * @return >0: 成功读出数据的长度，
 *         -1: 指针为空，
 *         -2: 队列为空,
 */
int8_t Fdcan_Queue_Pop_Send(p_fdcan_queue p_fdcan_queue)
{
	int8_t len = 0;
	
	len = Fdcan_Queue_Pop(p_fdcan_queue, fdcan2_tdata);
	if (len > 0)
	{
		CDC_Transmit_FS(fdcan2_tdata, len);
	}
	
	return len;
}

