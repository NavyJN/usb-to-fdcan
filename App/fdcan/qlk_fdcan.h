#ifndef _QLK_FDCAN_H
#define _QLK_FDCAN_H


#include "main.h"


#define  FDCAN_BUF_LEN     50  // 每个缓存的大小
#define  FDCAN_QUEUE_SIZE  10  // 


typedef struct
{
    uint8_t data[FDCAN_BUF_LEN];
    uint32_t id;
    uint8_t len;
} fdcan_buf;


typedef struct 
{
    fdcan_buf data[FDCAN_QUEUE_SIZE];  // 数据
    int front;  // 头指针
    int rear;   // 尾指针
} fdcan_queue, *p_fdcan_queue;

extern fdcan_queue fdcan_queue2;
extern p_fdcan_queue p_fdcan2_queue;
extern uint8_t fdcan2_rdata[FDCAN_BUF_LEN];
extern uint8_t fdcan2_tdata[FDCAN_BUF_LEN];


uint32_t Fdcan_Len_To_Dlc(uint8_t len);  // 根据数据长度计算 FDCAN 的 DLC
uint8_t Fdcan_Dlc_To_Len(uint32_t dlc);  // 跟据 FDCAN 的 DLC 计算数据长度
int8_t Fdcan_Queue_Push(fdcan_queue *fdcan_queue_x, uint8_t *data, uint8_t len);  // 将 FDCAN 数据入队
int8_t Fdcan_Queue_Pop(fdcan_queue *fdcan_queue_x, uint8_t *data);  // 将数据从队列中读出并放入 data，并返回读出数据的长度
int8_t Fdcan_Queue_Pop_Send(fdcan_queue *fdcan_queue_x);  // 从队列中取出数据并通过串口发送


#endif
